using Test
using LinearAlgebra
using MDProcessing

include("read_dump.jl")
include("interface.jl")
include("single_state_properties.jl")
