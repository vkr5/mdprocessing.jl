using MDProcessing

function test_gr(filename, rcut)
    system = read_dump(filename)
    gr = rdf(system, rcut, 50)
    return gr
end

function test_veldist(filename)
    system = read_dump(filename)
    vmin = -3.0
    vmax = 3.0
    dv = 0.1
    veldist = vel_distribution(system, vmin, vmax, dv)
    return veldist
end
