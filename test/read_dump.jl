@testset "Reading dump files" begin
    @test read_dump("lj.T1.d0.6.0.dump") isa MDState

    state_fresh = read_dump("lj.T1.d0.6.0.dump")
    state_overwrite = let state = read_dump("lj.T1.d0.7.0.dump")
        read_dump!("lj.T1.d0.6.0.dump", state; mode=:replace)
        state
    end
    @test all(
        state_fresh[p] == state_overwrite[p]
        for p in propertynames(state_fresh)
    )

    @test Set(propertynames(state_fresh)) == Set(propertynames(state_overwrite))

    @test all(
        hasproperty(state_fresh, p)
        for p in (:id, :type, :mol, :coord, :vel, :c_epot, :c_ekin)
    )

    @test all(
        length(state_fresh[p]) == length(state_fresh.coord)
        for p in setdiff(
            propertynames(state_overwrite),
            (:box_vectors, :cell_list, :origin, :pbc),
        )
    )

    @test all(
        length(state_overwrite[p]) == length(state_overwrite.coord)
        for p in setdiff(
            propertynames(state_overwrite),
            (:box_vectors, :cell_list, :origin, :pbc),
        )
    )

    @test natoms(state_fresh) / det(boxvectors(state_fresh)) ≈ 0.6
end
