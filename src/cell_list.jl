#=
TODO: support non-orthogonal cells
=#
function cell_list_drmin(system::MDState)
    cell_list = system.cell_list
    ssize = diag(system.box_vectors)
    return 2 * minimum(ssize ./ size(cell_list))
end

function cell_list_drmax(system::MDState)
    cell_list = system.cell_list
    ssize = diag(system.box_vectors)
    return 2 * maximum(ssize ./ size(cell_list))
end

function build_cell_list!(system::MDState)
    if isempty(system.cell_list)
        return system
    end
    cell_list = system.cell_list
    if !isdiag(system.box_vectors)
        throw(ArgumentError("Non-orthogonal cells are not supported"))
    end
    boxsize = diag(system.box_vectors)
    coords = system.coord
    celldims = size(cell_list)
    empty!.(cell_list)
    for idx in eachindex(coords)
        r = coords[idx]
        buf = floor.(Int, mod.(r ./ boxsize, 1) .* celldims) .+ 1
        push!(cell_list[buf[1], buf[2], buf[3]], idx)
    end
    return system
end

function build_cell_list!(system::MDState, rneigh::Real)
    if !isdiag(system.box_vectors)
        throw(ArgumentError("Non-orthogonal cells are not supported"))
    end
    boxsize = diag(system.box_vectors)
    rdoub = 2 * rneigh
    for ldim in boxsize
        if rdoub > ldim
            throw(ArgumentError("Neighbor radius $rneigh exceeds half of box size $ldim"))
        end
    end
    celldims = floor.(Int, 2 .* boxsize ./ rneigh) |> Tuple
    if all(x -> 1 <= x < cbrt(2), celldims ./ size(system.cell_list))
        return system
    end
    cell_list = [Int[] for _ in CartesianIndices(celldims)]
    coords = system.coord
    for idx in eachindex(coords)
        r = coords[idx]
        buf = floor.(Int, mod.(r ./ boxsize, 1) .* celldims) .+ 1
        push!(cell_list[buf[1], buf[2], buf[3]], idx)
    end
    system.cell_list = cell_list
    return system
end

"""
    neigh_atoms(system::MDState, cell_inds)

Return the array with indices of atoms in cell `cell_inds` and the neighboring cells,
sorted in the ascending order. `cell_inds` may be a tuple or `CartesianIndices`.
"""
function neigh_atoms(system::MDState, cell_inds::CartesianIndex)
    cell_list = system.cell_list
    lsize = size(cell_list)
    ix, iy, iz = Tuple(cell_inds)
    ixl, iyl, izl = (ix, iy, iz) .- (lsize .> 4) .- 1
    ixr, iyr, izr = (ix, iy, iz) .+ 2

    clamp(ind, imax) = ind > imax ? ind - imax : ind > 0 ? ind : ind + imax
    neighs = sizehint!(
        similar(cell_list[cell_inds], 0),
        (ixr-ixl+1) * (iyr-iyl+1) * (izr-izl+1) * ceil(Int, natoms(system) / prod(lsize))
    )
    @inbounds for jz in izl:izr, jy in iyl:iyr, jx in ixl:ixr
        append!(neighs, cell_list[clamp.((jx, jy, jz), lsize)...])
    end
    return sort!(neighs)
end
