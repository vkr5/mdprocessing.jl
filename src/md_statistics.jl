using Statistics

"""
    natoms(system::MDState)

Return the number of atoms in the system.
"""
natoms(system) = length(system.coord)

"""
    boxvectors(system::MDState)

Return the 3x3 matrix of box basis vectors.
"""
boxvectors(system::MDState) = system.box_vectors

"""
    boxorigin(system::MDState)

Return the vector of the box coordinate origin.
"""
boxorigin(system::MDState) = system.origin

"""
    msd(state, ref_state)

Compute the mean square displacement of particles in `state` relative to `ref_state`.
"""
function msd(state::MDState{T}, ref_state::MDState{T}) where {T}
    if state.id != ref_state.id
        error("Cannot compute MSD: IDs do not coincide")
    end

    dr2 = zero(T)
    coord = state.coord
    ref_coord = ref_state.coord
    scale = inv(natoms(ref_state))
    for idx in eachindex(coord, ref_coord)
        Δr = coord[idx] - ref_coord[idx]
        dr2 += mag2(Δr) * scale
    end
    return dr2
end

"""
    vel_distribution!(dist, system, vmin, vmax, dv)

Compute distribution of velocity components in the range from `vmin` to `vmax`
with the step `dv` and overwrite `dist` with it.
"""
function vel_distribution!(
    dist::AbstractVector, system::MDState{T}, vmin, vmax, dv
) where {T}
    vel = system.vel
    dist .= 0
    for iatom in 1:length(vel), i in 1:3
        nbin = floor(Int, (vel[iatom][i] - vmin) / dv) + 1
        if nbin in eachindex(dist)
            dist[nbin] += 1
        end
    end
    dist ./= sum(dist)
    return dist
end

"""
    vel_distribution(system, vmin, vmax, dv)

Compute distribution of velocity components in the range from `vmin` to `vmax`
with the step `dv`.
"""
function vel_distribution(system::MDState, vmin, vmax, dv)
    n_distr_bins = ceil(Int, (vmax - vmin) / dv)
    distr = zeros(Float64, n_distr_bins)
    return vel_distribution!(distr, system, vmin, vmax, dv)
end

"""
    rdf!(gr, system, rmax, nslice)

Compute radial distribution function (RDF) over all atoms in `system` and overwrite `gr`
    with it. `rmax` defines the cutoff radius for the computed RDF, the length or number of
    rows in `gr` defines the number of histogram bins.

# Arguments
- `gr::AbstractVecOrMat`: the array to write the RDF to. If `gr isa AbstractVector`, then
    only g(r) values are written. If `gr isa AbstractMatrix`, then the the coordinates of
    histogram bin centers are written into the first column, the g(r) values to the second
    column
- `system::MDState`: the system for which the RDF is calculated
- `rmax::Real`: radius up to which the RDF is calculated

# Returns
- `AbstractVector`: the array `gr` overwritten by the RDF
"""
function rdf!(gr::AbstractVector, system::MDState, rmax::Real)
    nslice = length(gr)
    build_cell_list!(system, rmax)
    coord = system.coord
    cell_list = system.cell_list
    dr = rmax / nslice
    invdr = inv(dr)
    boxsize = diag(boxvectors(system))
    V = prod(boxsize)
    gr .= 0
    @inbounds for i in CartesianIndices(cell_list)
        neigh_i = neigh_atoms(system, i)
        for atom1 in cell_list[i]
            r1 = coord[atom1]
            iatom1 = searchsortedlast(neigh_i, atom1)
            for atom2 in @view neigh_i[iatom1+1:end]
                r2 = coord[atom2]
                Δr12sq = dist2(r1, r2, boxsize)
                if Δr12sq < rmax^2
                    slice_index = floor(Int, sqrt(Δr12sq) * invdr) + 1
                    gr[slice_index] += 1
                end
            end
        end
    end
    ntotal = natoms(system)
    density = ntotal / V
    vol_prefactor = 4/3 * π * dr^3
    for i in eachindex(gr)
        vol_i = vol_prefactor * (i^3 - (i-1)^3)
        npair_ref = density * vol_i
        gr[i] /= ntotal * npair_ref / 2
    end
    return gr
end

function rdf!(gr::AbstractMatrix, system::MDState, rmax::Real)
    nbins = size(gr, 1)
    dr = rmax / nbins
    rrange = dr/2 : dr : rmax
    @views gr[:, 1] .= rrange
    rdf!(@view(gr[:, 2]), system, rmax)
    return gr
end

"""
    rdf(system, rmax, nslice)

Compute radial distribution function (RDF) over all atoms in `system`.

# Arguments
- `system::MDState`: the system for which the RDF is calculated
- `rmax::Real`: radius up to which the RDF is calculated
- `nslice::Integer`: number of slices in the RDF

# Returns
- `Matrix{Float64}`: the computed RDF. The first column stores the centers of histogram
    bins, the second column stores the RDF values.
"""
function rdf(system::MDState, rmax::Real, nslice::Integer)
    return rdf!(zeros(Float64, nslice, 2), system, rmax)
end

"""
    fourpoint_cc(system, systemnext, rbracket)

Compute the correlation coefficient between displacements of atom pairs in `system` and
    `systemnext` that are within `rbracket` apart in `system`.

# Arguments
- `system::MDState`: the base system
- `systemnext::MDState`: the system with displaced atoms
- `rbracket::Tuple`: the distance bracket in the form `(rmin, rmax)`

# Returns
- `Float64`: the correlation coefficient
"""
function fourpoint_cc(
    system::MDState,
    systemnext::MDState,
    rbracket::Tuple{Real,Real},
)
    rlo, rhi = rbracket
    build_cell_list!(system, rhi)
    coord = system.coord
    coordnext = systemnext.coord
    dcoord = coordnext .- coord
    cell_list = system.cell_list
    boxsize = diag(system.box_vectors)
    accum = 0.0
    pair_count = 0
    for idx in CartesianIndices(cell_list)
        neigh_i = neigh_atoms(system, idx)
        for i in cell_list[idx]
            r1 = coord[i]
            d1 = dcoord[i]
            for j in Iterators.reverse(neigh_i)
                j > i || break
                r2 = coord[j]
                d2 = dcoord[j]
                Δr12 = dist(r1, r2, boxsize)
                if rlo < Δr12 < rhi
                    accum += coss(d1, d2)
                    pair_count += 1
                end
            end
        end
    end
    return pair_count == 0 ? 0.0 : accum / pair_count
end


"""
    fourpoint_cc(system, systemnext; r₀, thickness)

Compute the correlation coefficient between displacements of atom pairs in `system` and
    `systemnext` for which the distances are in the interval
    `(r₀ - thickness/2, r₀ + thickness/2)`.

# Arguments
- `system::MDState`: the base system
- `systemnext::MDState`: the system with displaced atoms

# Keywords
- `r₀`: the median radius of the spherical layer
- `thickness`: the thickness of the spherical layer

# Returns
- `Float64`: the correlation coefficient
"""
function fourpoint_cc(
    system::MDState,
    systemnext::MDState;
    r₀::Real,
    thickness::Real,
)
    rbracket = (r₀ - thickness / 2, r₀ + thickness / 2)
    return fourpoint_cc(system, systemnext, rbracket)
end

function fourpoint_cc_rsweep!(
    accum::AbstractVector,
    lengths::AbstractVector,
    system::MDState,
    systemnext::MDState;
    rmax::Real,
    nbin::Real,
)
    build_cell_list!(system, rmax)
    coord = system.coord
    coordnext = systemnext.coord
    cell_list = system.cell_list
    boxsize = system.size
    dr = rmax / nbin
    for idx in CartesianIndices(cell_list)
        neigh_i = neigh_atoms(system, idx)
        for i in cell_list[idx]
            r1 = coord[i]
            d1 = coordnext[i] - r1
            for j in Iterators.reverse(neigh_i)
                j > i || break
                r2 = coord[j]
                Δr12 = dist(r1, r2, boxsize)
                if Δr12 < rmax
                    d2 = coordnext[j]
                    slice_index = floor(Int, Δr12 / dr) + 1
                    accum[slice_index] += coss(d1, d2)
                    lengths[slice_index] += 1
                end
            end
        end
    end
    map!(accum, accum, lengths) do (acc, n)
        ave = acc / n
        return isnan(ave) ? zero(ave) : ave
    end
    return accum
end

function fourpoint_cc_rsweep(
    system::MDState{T},
    systemnext::MDState{T};
    rmax::Real,
    nbin::Real,
) where {T}
    accum = similar(system.coord, T)
    lengths = similar(accum, Int)
    return fourpoint_cc_rsweep!(accum, lengths, system, systemnext; rmax, nbin)
end

"""
    mol_com(system::MDState)

Return an `MDState` with the same box bounds as `system` with particles located at the
    center-of-mass positions for all molecules in `system` and having the center-of-mass
    velocities of molecules.
"""
function mol_com(system::MDState{T}) where T
    buf = MDState{T}(; boxsize=system.size)
    mol = system.mol
    mass = system.mass
    coord = system.coord
    vel = system.vel
    mol_ids = sort!(unique(mol))
    __mol_com!(buf, mol, mol_ids, mass, coord, vel)
    return buf
end

#=
Fill coordinates and IDs in `buf` with vectors of centers of mass of molecules and molecule
IDs. Add property `mass` to `buf` with the values of molecular masses.
Arguments:
buf: state to overwrite
mol: vector of molecule IDs in the original system
mol_ids: vector of unique molecule IDs, sorted in the ascending order
mass: vector of atom masses
coord: vector of atom coords
vel: vector of atom velocities
=#
function __mol_com!(
    buf::MDState{T},
    mol,
    mol_ids,
    mass,
    coord::VecVec{T},
    vel::VecVec{T}
) where {T}
    num_mol = length(mol_ids)
    update_coord!(buf, SVector(0.0, 0.0, 0.0) for _ in 1:num_mol)
    cmass = buf.coord
    vcom = buf.vel
    molmass = zeros(T, length(cmass))
    if last(mol_ids) == num_mol
        # all molecule numbers 1:num_mol are present
        for iatom in eachindex(coord)
            imol = mol[iatom]
            cmass[imol] += mass[iatom] * coord[iatom]
            vcom[imol] += mass[iatom] * vel[iatom]
            molmass[imol] += mass[iatom]
        end
    else
        for iatom in eachindex(coord)
            mol_id = mol[iatom]
            imol = searchsortedfirst(mol_ids, mol_id)
            cmass[imol] += mass[iatom] * coord[iatom]
            vcom[imol] += mass[iatom] * vel[iatom]
            molmass[imol] += mass[iatom]
        end
    end
    cmass ./= molmass
    vcom ./= molmass
    buf.id .= mol_ids
    buf.mol .= mol_ids
    if hasproperty(buf, :mass)
        buf.mass .= molmass
    else
        addproperty!(buf; name="mass", data=molmass, allow_sharing=true)
    end
    return buf
end

function c_mass_for_mol(coords, mass)
    cmass = zero(eltype(coords))
    molmass = 0.0
    for idx in 1:length(coords)
        cmass += mass[idx] * coords[idx]
        molmass += mass[idx]
    end
    cmass = cmass / molmass
    return cmass
end

"""
    rdfw!(gr, system, rmax, nslice; weights::AbstractVector)

Compute radial distribution function using `weights` as weights for atom types and
    overwrite `gr` with it.
"""
function rdfw!(
    gr::AbstractVector, system::MDState, rmax::Real;
    weights::AbstractVector
)
    build_cell_list!(system, rmax)
    nslice = length(gr)
    coord = system.coord
    cell_list = system.cell_list
    dr = rmax / nslice
    boxsize = diag(boxvectors(system))
    ntotal = 0
    V = prod(boxsize)
    gr .= 0
    for i in CartesianIndices(cell_list)
        neigh_i = neigh_atoms(system, i)
        for atom1 in cell_list[i]
            w1 = get(weights, system.type[atom1], nothing)
            w1 == 0 || w1 === nothing && continue
            r1 = coord[atom1]
            ntotal += w1
            iatom1 = searchsortedlast(neigh_i, atom1)
            for atom2 in @view neigh_i[iatom1+1:end]
                w2 = get(weights, system.type[atom2], nothing)
                w2 == 0 || w2 === nothing && continue
                r2 = coord[atom2]
                Δr12 = dist(r1, r2, boxsize)
                if Δr12 < rmax
                    slice_index = floor(Int, Δr12 / dr) + 1
                    gr[slice_index] += w1 * w2
                end
            end
        end
    end
    density = ntotal / V
    prefactor = 4/3 * π * dr^3
    for i in eachindex(gr)
        vol_i = prefactor * (i^3 - (i-1)^3)
        npair_ref = density * vol_i
        gr[i] /= ntotal * npair_ref / 2
    end
    return gr
end

function rdfw!(gr::AbstractMatrix, system::MDState, rmax::Real; weights)
    nbins = size(gr, 1)
    dr = rmax / nbins
    rrange = dr/2 : dr : rmax
    @views gr[:, 1] .= rrange
    rdfw!(@view(gr[:, 2]), system, rmax; weights)
    return gr
end

"""
    rdfw(system, rmax, nslice; weights)

Compute radial distribution function with weights.
"""
function rdfw(
    system::MDState, rmax::Real, nslice::Integer;
    weights::AbstractVector{<:Real}
)
    return rdfw!(zeros(Float64, nslice, 2), system, rmax; weights)
end

"""
    normal_calc(coords)

Compute the normal to the plane inscribed by the least squares method in the point cloud.
"""
function normal_calc(coords, p)
    A = foldl(hcat, coords; init=similar(coords[1], length(coords[1]), 0))
    A .-= p
    nor = svd(A').Vt[3, :]
    sign = dot3(@view(A[:, 1]), @view(A[:, 2]), nor)
    if sign < 0
        nor .*= -1
    end
    return nor
end

"""
    plane(system, molecules, atoms)

Return an `MDState` with the same box bounds as `system` with particles located at the
    center-of-mass positions for molecules in `system` with IDs in `molecules`. Particles
    get a vector property `:normal` which is the result of `normal_calc` for each molecule.
    Additionally, `atoms` can be passed to compute COMs and normals for specific subsets of
    atoms in each molecule. For example, if `atoms = 1:10`, then only first 10 atoms (by ID)
    in each molecule are used for computations.
"""
function plane(system::MDState, molecules::AbstractVector, atoms=nothing)
    buf = MDState(; boxsize=system.size)
    coord = system.coord
    cmass = empty(coord)
    M = Dict(mol=>empty(coord) for mol in molecules) #dict {mol_num : coords of its atoms}
    for (imol, r) in zip(system.mol, coord)
        if imol in keys(M)
            push!(M[imol], r)
        end
    end
    Masses = Dict(mol=>empty(system.mass) for mol in molecules)
    for (imol, m) in zip(system.mol, system.mass)
        if imol in keys(M)
            push!(Masses[imol], m)
        end
    end
    cmass = similar(molecules, SVector{3,Float64})
    normals = similar(cmass)

    for (i, imol) in enumerate(molecules)
        rs = atoms === nothing ? M[imol] : view(M[imol], atoms)
        ms = atoms === nothing ? Masses[imol] : view(Masses[imol], atoms)
        com = dot(rs, ms) / sum(ms)
        normals[i] = normal_calc(rs, com)
        cmass[i] = com
    end
    update_coord!(buf, cmass)
    addproperty!(buf; name=:normal, data=normals)
    return buf
end

function plane(dump_file, molecules, atoms)
    system = read_dump(dump_file)
    return plane(system, molecules, atoms)
end

"""
    line(system, molecules, atoms)

Return a vector of center-of-mass positions for given molecules and atoms in `system`
and a result of line_calc for given molecules and atoms in `system`
"""
function line(system, molecules, atoms)
    coord = system.coord
    cmass = empty(coord)
    M = Dict(mol=>empty(coord) for mol in molecules)
    for (imol, r) in zip(system.mol,coord)
        if imol in keys(M)
            push!(M[imol], r)
        end
    end
    Ma = Dict(mol=>empty(system.mass) for mol in molecules)
    for (imol, r) in zip(system.mol,system.mass)
        if imol in keys(M)
            push!(Ma[imol], r)
        end
    end
    res = Vector{Float64}[]
    for imol in keys(M)
        rs = view(M[imol], atoms)
        ms = view(Ma[imol], atoms)
        cmm = dot(rs, ms) / sum(ms)
        push!(res, line_calc(rs, cmm))
        push!(cmass, cmm)
    end
    return cmass, res
end

"""
    line_calc(coords)

Compute the guide to the straight inscribed in the point cloud.
"""
function line_calc(coords, p)
    A = zeros(3, length(coords))
    for idx in 1:length(coords)
        r = coords[idx]
        A[:, idx] .= r
    end
    A .-= p
    U,S,V = svd(A')
    n = V[:,1]
    return n
end

function correlate!(buf, vecs0, vecs1)
    buf .= dot.(vecs0, vecs1)
    return buf
end

function corr_normals(system0, system1, molecules, atoms)
    _, nrm0 = plane(system0, molecules, atoms)
    _, nrm1 = plane(system1, molecules, atoms)
    return correlate!(similar(molecules, Float64), nrm0, nrm1)
end

"""
    traj_correlate(corr_func::Function, dir::AbstractString, fmask::AbstractString, dt::Integer)

Correlate a property computed by `corr_func(state, reference)` over an MD trajectory. The
    trajectory is written in files stored in `dir` corresponding to mask `fmask`. The
    correlation between states separated by `dt` is computed (the separation is inferred
    from the file names).
"""
function traj_correlate(corr_func::Function, direct::AbstractString, mask::AbstractString, dt::Integer)
    system = nothing
    systemref = nothing
    newmask = split(mask, '*')
    accum = nothing
    n_dumps = 0
    for fname in readdir(direct)
        if startswith(fname, newmask[1]) && endswith(fname, newmask[2])
            num = fname[sizeof(newmask[1])+1:end-sizeof(newmask[2])]
            numt = tryparse(Int, num) + dt
            mask2 = string(newmask[1], numt, newmask[2])
            refname = joinpath(direct, fname)
            compname = joinpath(direct, mask2)
            if isfile(compname)
                n_dumps += 1
                systemref =
                    systemref === nothing ?
                    read_dump(refname) :
                    read_dump!(refname, systemref)
                system =
                    system === nothing ?
                    read_dump(compname) :
                    read_dump!(compname, system)
                corr = corr_func(system, systemref)
                if accum === nothing
                    accum = copy(corr)
                elseif isimmutable(accum)
                    accum = accum .+ corr
                else
                    accum .+= corr
                end
            end
        end
    end
    return accum / n_dumps
end

"""
    msdt(dir::AbstractString, mask::AbstractString, dt::Integer)

Compute the average molecular MSD for all dumps in `dir` that match `mask` and are `dt` time steps apart.
`mask` has the form `file.*.extension` where `*` is the timestep number
(following the LAMMPS naming convention).
"""
function msdt(direct::AbstractString, mask::AbstractString, dt::Integer)
    return traj_correlate(direct, mask, dt) do system, systemref
        molec_com = mol_com(system)
        molec_com_ref = mol_com(systemref)
        return msd(molec_com, molec_com_ref)
    end
end

function correlate_normals(direct::AbstractString, mask::AbstractString, dt::Integer, molecules, atoms)
    dot_buf = similar(molecules, Float64)
    function nrm_corr(system, systemref)
        buf = plane(system, molecules, atoms)
        buf_ref = plane(systemref, molecules, atoms)
        dot_buf .= dot.(buf.normal, buf_ref.normal)
        return mean(dot_buf)
    end
    return traj_correlate(nrm_corr, direct, mask, dt)
end

function correlate_normals_dt(direct::AbstractString, mask::AbstractString, dt::AbstractVector, molecules, atoms, output)
    for t in dt
        println(output, t, ' ', correlate_normals(direct, mask, t, molecules, atoms))
    end
end

"""
    rdf_wrapper(system::State, rmax::Real, nslice::Integer, molecules, atoms)

Return a result of rdf calculated on a set of molecules and atoms.
"""
function rdf_wrapper(system::MDState, rmax::Real, nslice::Integer, molecules, atoms)
    coord = system.coord
    molsystem = MDState(; boxsize=system.size)
    M = Dict(mol=>empty(coord) for mol in molecules)
    for (imol, r) in zip(system.mol,coord)
        if imol in keys(M)
            push!(M[imol], r)
        end
    end
    for imol in keys(M)
        append!(molsystem.coord, view(M[imol], atoms))
    end
    ro = length(M)*length(atoms)/prod(system.size)
    return rdf(molsystem, rmax, nslice), ro
end

"""
    entropy(ro, gr, rmax)

Compute the paired contribution to entropy from the paired carbon-carbon correlation function.
"""
function entropy(ro, gr, rmax)
    dr = rmax / length(gr)
    prefactor(g) = g == 0 ? one(float(g)) : g * log(g) - g + 1
    r = range(dr/2, length=length(gr), step=dr)
    input = prefactor.(gr) .* r.^2
    integ = integrate_trap(input, dr)
    integ .*= -2 * pi * ro
    return integ
end
