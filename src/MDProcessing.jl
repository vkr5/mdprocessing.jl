module MDProcessing

using LinearAlgebra
using StaticArrays
using Printf: @format_str, Format, format
using Base.Sort

export MDState
export addproperty!, update_coord!, clear_state!
export natoms, boxvectors, boxorigin
export read_dump, read_dump!
export rdf, rdf!, rdfw, rdfw!, kin_energy, msd, vel_distribution, vel_distribution!
export msdt
export traj_average, traj_correlate
export Autocorr

include("types.jl")
include("iteration.jl")
include("cell_list.jl")
include("math.jl")
include("md_statistics.jl")
include("io_utils.jl")
include("autocorr/autocorr.jl")

end # module MDProcessing
