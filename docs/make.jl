using Documenter, MDProcessing

makedocs(;
    modules=[MDProcessing, MDProcessing.Autocorr],
    checkdocs=:exports,
    sitename="MDProcessing.jl documentation",
    format = Documenter.HTML(
        prettyurls = get(ENV, "CI", nothing) == "true"
    ),
    repo = "https://gitlab.com/pisarevvv/mdprocessing.jl/blob/{commit}{path}#{line}"
)
