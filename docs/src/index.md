# MDProcessing.jl documentation

## Type constructors
```@docs
MDState
```

## Basic interface
```@docs
natoms

Base.length(::MDState)

MDParticle

Base.filter(fn, ::MDState)
```

## Adding a property

```@docs
addproperty!
```

## Reading data

```@docs
read_dump

read_dump!
```

## Trajectory functions

```@docs
traj_average

traj_correlate

msdt
```

## Autocorrelation

The functions below are part of `MDProcessing.Autocorr` module.

```@docs
Autocorr.acf_fft

Autocorr.integrate
```

## Other analysis functions

```@docs
vel_distribution

vel_distribution!

msd

msdt

rdf

rdf!

rdfw

rdfw!

rdf_wrapper

entropy
```
