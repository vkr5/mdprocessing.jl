# MDProcessing.jl

Package for post-processing of molecular dynamics trajectories.

Currently, support is limited to [LAMMPS](https://lammps.org) [dump files](https://docs.lammps.org/dump.html).

## Installation

```julia
using Pkg

pkg"registry add https://gitlab.com/pisarevvv/samma-registry"

pkg"add MDProcessing"
```

See the [documentation page](https://pisarevvv.gitlab.io/mdprocessing.jl/) for the interface.

The development repository for versions 0.4.1 and prior is https://gitlab.com/vkr5/mdprocessing.jl